﻿/**
 * 复制bower包的主要文件到指定目录
 * 
 * 文件源和目标路径配置
   "overrides": {
    
    }
 */
var test = {
    "overrides": {//节点名称
        "bootstrap": {//包名
            "./dist/js/bootstrap.*": "js/",//文件名=>目标文件夹（复制到包名目录下用“./”）
            "./dist/css/*.*": "css/",
            "./dist/fonts/*.*": "fonts/"
        }
    }
}


var fs = require('fs');
var path = require('path');
var vfs = require('vinyl-fs');

function getBowerFolder(base) {
    var rcPath = path.join(base, '.bowerrc');
    if (fs.existsSync(rcPath)) {
        var config = JSON.parse(fs.readFileSync(rcPath));
        if (config.directory) {
            return config.directory;
        }
    }
    return 'bower_components/';
}

function copy(base, output) {
    try {
        //获取bower配置文件
        var configPath = path.join(base, 'bower.json');
        var config = JSON.parse(fs.readFileSync(configPath));
        //bower包位置
        var bowerFolder = path.join(base, getBowerFolder(base));
        //输出位置
        var dest = path.join(base, output);
        //输出配置
        var overrides = config.overrides;

        for (var pkName in overrides) {
            if (overrides.hasOwnProperty(pkName)) {
                var group = overrides[pkName];
                for (var g in group) {
                    if (group.hasOwnProperty(g)) {
                        var tar = group[g];
                        var src = path.join(bowerFolder, pkName, g);
                        src = src.replace(/\\/g,"/");//反斜杠转正斜杠，否则输出路径有问题
                        var target = path.join(dest, pkName, tar);

                        //得到文件路径和输出路径后开始复制文件
                        console.log(src + " => " + target);
                        vfs.src(src).pipe(vfs.dest(target));
                    }
                }
            }
        }
        console.log("复制完成");
    } catch (e) {
        console.log(e.message);
    }
}

module.exports = copy;

var cp = require("./copy.js");

//根目录
var base = './';
//输出路径
var output = 'wwwroot/libs/';

cp(base, output);