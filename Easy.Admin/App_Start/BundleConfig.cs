﻿using System.Configuration;
using System.Web;
using System.Web.Optimization;

namespace Easy.Admin
{
    public class BundleConfig
    {
        public static readonly string Wwwroot = ConfigurationManager.AppSettings["wwwrootLibsPath"] ?? "~/wwwroot/libs/";
        // 有关捆绑的详细信息，请访问 https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/animate").Include(
                Wwwroot + "animate.css/animate.css"));
            
            bundles.Add(new StyleBundle("~/blueimp-gallery/css").Include(
                Wwwroot + "blueimp-gallery/css/blueimp-gallery.css"));
            bundles.Add(new ScriptBundle("~/blueimp-gallery/js").Include(
                Wwwroot + "blueimp-gallery/js/blueimp-gallery.js"
            ));

            bundles.Add(new StyleBundle("~/bootstrap/css").Include(
                Wwwroot + "bootstrap/css/bootstrap.css"));
            bundles.Add(new ScriptBundle("~/bootstrap/js").Include(
                Wwwroot + "bootstrap/js/bootstrap.js",
                Wwwroot + "popper.js/umd/popper.js"
                ));

            bundles.Add(new StyleBundle("~/bootstrap-table/css").Include(
                Wwwroot + "bootstrap-table/bootstrap-table.css"));
            bundles.Add(new ScriptBundle("~/bootstrap-table/js").Include(
                Wwwroot + "bootstrap-table/bootstrap-table.js"));

            bundles.Add(new StyleBundle("~/font-awesome").Include(
                Wwwroot + "font-awesome/css/font-awesome.css"));

            bundles.Add(new StyleBundle("~/iCheck/css").Include(
                Wwwroot + "iCheck/skins/all.css"));
            bundles.Add(new ScriptBundle("~/iCheck/js").Include(
                Wwwroot + "iCheck/icheck.js"));

            bundles.Add(new ScriptBundle("~/jquery").Include(
                Wwwroot + "jQuery/jquery.js"));

            bundles.Add(new ScriptBundle("~/jquery-slimscroll").Include(
                Wwwroot + "jquery-slimscroll/jquery.slimscroll.js"));

            bundles.Add(new ScriptBundle("~/laydate").Include(
                Wwwroot + "laydate/laydate.js"));

            bundles.Add(new ScriptBundle("~/layer").Include(
                Wwwroot + "layer/layer.js"));

            bundles.Add(new StyleBundle("~/metisMenu/css").Include(
                Wwwroot + "metisMenu/metisMenu.css"));
            bundles.Add(new ScriptBundle("~/metisMenu/js").Include(
                Wwwroot + "metisMenu/metisMenu.js"));

            bundles.Add(new ScriptBundle("~/pace").Include(
                Wwwroot + "PACE/pace.js"));

            bundles.Add(new StyleBundle("~/zTree/css").Include(
                Wwwroot + "zTree/css/metroStyle/metroStyle.css"));
            bundles.Add(new ScriptBundle("~/zTree/js").Include(
                Wwwroot + "zTree/js/jquery.ztree.all.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            //            "~/Scripts/jquery.validate*"));

            //// 使用要用于开发和学习的 Modernizr 的开发版本。然后，当你做好
            //// 生产准备就绪，请使用 https://modernizr.com 上的生成工具仅选择所需的测试。
            //bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
            //            "~/Scripts/modernizr-*"));

            //bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
            //          "~/Scripts/bootstrap.js",
            //          "~/Scripts/respond.js"));

            //bundles.Add(new StyleBundle("~/Content/css").Include(
            //          "~/Content/bootstrap.css",
            //          "~/Content/site.css"));
        }
    }
}
