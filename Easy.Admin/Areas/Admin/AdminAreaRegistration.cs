﻿using System.ComponentModel;
using System.Web.Mvc;

namespace Easy.Admin.Areas.Admin
{
    [DisplayName("系统管理")]
    public class AdminAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return nameof(AdminAreaRegistration).Replace("AreaRegistration",""); 
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                AreaName,
                AreaName+"/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                new[] { typeof(AdminAreaRegistration).Namespace+".Controllers" }
            );
        }
    }
}