﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Gxu.Models;

namespace Easy.Admin.Controllers
{
    [DisplayName("系统管理")]
    public class ManagerController : Controller
    {
        // GET: Manager
        public ViewResult Index()
        {
            return View();
        }

        // GET: Manager
        public ViewResult Test()
        {
            return View();
        }

        // GET: /Menu/
        /// <summary>
        /// 获取菜单
        /// </summary>
        /// <returns></returns>
        public ActionResult GetMenu()
        {
            var list = Menus.FindAll(Menus._.Remove == false, "", "", 0, 0).ToList();

            return Json(Menus.ToTreeEntities(list));

        }
        //
        // GET: /Menu/
        ///// <summary>
        ///// 获取菜单
        ///// </summary>
        ///// <returns></returns>
        //public ActionResult GetAllMenu()
        //{
        //   var list = Menus.FindAll("Remove", false);

        //    //list = Menu.FindAll("Remove = 0", "Depth,Sort,ID", "", 0, 0);
        //    List<TreeEntity> models = new List<TreeEntity>();
        //    var lst = list.OrderBy(f => f.ParentID).OrderBy(f => f.Sort).ToList();
        //    foreach (var n in lst)
        //    {
        //        if (n.Remove)
        //            continue;
        //        var tree = new TreeEntity()
        //        {
        //            complete = true,
        //            id = n.ID.ToString(),
        //            //hasChildren = n.HasChilds,
        //            hasChildren = n.IsChild,
        //            //name = n.ResourceName,
        //            name = n.Name,
        //            //value = n.ResourceUrl,
        //            value = n.ID.ToString(),
        //            //url = "../" + n.ResourceUrl,
        //            target = "frmright",
        //            showProgess = false,
        //            parentId = n.ParentID.ToString(),
        //            ActionIDs = n.ActionIDs
        //            //parentId = n.Pid.ToString()
        //        };
        //        models.Add(tree);
        //    }

        //    Hashtable has = new Hashtable();
        //    has.Add("treeNodes", models);
        //    return Json(has);
        //}
        //[HttpPost]
        //public ActionResult getActionDataList()
        //{
        //    var list = Actions.FindAll("IsGroup=0", "ParentID,ID", "", 0, 0);
        //    Hashtable result = new Hashtable();
        //    result.Add("pager.pageNo", 1);
        //    result.Add("rows", list);
        //    result.Add("pager.totalRows", list.Count);
        //    return Json(result);
        //}
        //[HttpPost]
        //[ActionDesc("保存菜单操作关联", "允许保存菜单操作关联", groupName: "系统管理")]
        //[AJAXResult]
        //public ActionResult SaveMenu(Menus m)
        //{
        //    if (m.ID > 0 && !string.IsNullOrWhiteSpace(m.ActionIDs))
        //    {
        //        var ids = m.ActionIDs.Split(",");
        //        MenuActionMap.Delete(new string[] { "MenuID" }, new object[] { m.ID });
        //        foreach (var item in ids)
        //        {
        //            int id = 0;
        //            if (int.TryParse(item, out id))
        //            {
        //                MenuActionMap ma = new MenuActionMap();
        //                ma.ActionID = id;
        //                ma.MenuID = m.ID;
        //                ma.Insert();
        //            }
        //        }
        //    }
        //    return Json(new AjaxResult("保存成功", true));
        //}
        ///// <summary>
        ///// 注册菜单
        ///// </summary>
        ///// <returns></returns>
        //[ActionDesc("注册菜单", "设置系统菜单信息")]
        //public ActionResult RegisterMenu()
        //{
        //    //获取所有程序集中类型名包含 Controller 的类型
        //    var allTypes = AppDomain.CurrentDomain.GetAssemblies().SelectMany(sm => sm.GetTypes().Where(w => w.Name.EndsWith("Controller"))).ToArray();

        //    //通过ActionDescAttribute MenuDescAttribute 特性过滤并构造菜单实体
        //    var menuEntities = MenuDescManage.GetAllMenus(allTypes);

        //    //根据GroupName 分组
        //    var groupMenuEntities = menuEntities.GroupBy(f => f.GroupName);

        //    //存放所有菜单
        //    var menus = new List<Menus>(menuEntities.Count);

        //    //一个ActionEntity对应的多个菜单 形式的字典
        //    var dic = new Dictionary<ActionEntity, List<Menus>>();

        //    //处理菜单分组
        //    foreach (var item in groupMenuEntities)
        //    {
        //        //取第一条菜单实体
        //        var pm = menuEntities.FirstOrDefault(w => w.Name.EqualIgnoreCase(item.Key));

        //        Func<MenuEntity, bool> c = (o) => true;

        //        // - 分割分组名，按顺序构成多级菜单
        //        var menukey = item.Key.Split('-');

        //        int num = menukey.Count();

        //        //父级菜单，此变量每复制一次，就将以上 - 分割的分组名组合成多级菜单关联
        //        var parentMenu = new Menus();

        //        //记录一个ActionEntity对应的所有菜单
        //        var amenu = new List<Menus>();

        //        if (pm?.ActionEntity != null)
        //        {
        //            if (dic.ContainsKey(pm.ActionEntity))
        //            {
        //                amenu = dic[pm.ActionEntity];
        //            }
        //        }

        //        //根据 - 分割key的数组，循环产生顶级菜单，前一个作为后一个的父级，产生三级菜单，多级菜单设置多个 - 连接即可
        //        for (int i = 0; i < num; i++)
        //        {
        //            var cur = new Menus
        //            {
        //                Name = menukey[i],//item.Key,
        //                //State = State.Normal,
        //                ChildNum = 1,
        //                Sort = pm?.Order ?? 0,
        //                Parent = parentMenu,//这个parentMenu可能是前一个cur，因为class是引用类型，所以可以构成多级引用的菜单
        //            };
        //            if (!amenu.Contains(cur))
        //                amenu.Add(cur);
        //            menus.Add(cur);
        //            parentMenu = cur;
        //        }

        //        //循环处理该key下的所有菜单
        //        foreach (var subItem in item)
        //        {
        //            parentMenu.Render = subItem.Render;
        //            parentMenu.IsSys = subItem.IsSys;
        //            var subMenu = new Menus
        //            {
        //                Name = subItem.Name,
        //                Description = subItem.Desc,
        //                URL = subItem.Url,
        //                Controller = subItem.Controller,
        //                Action = subItem.Action,
        //                Parameters = subItem.Parameters,
        //                //State = State.Normal,
        //                Parent = parentMenu,
        //                Sort = subItem.Order,
        //                Render = subItem.Render,
        //                IsSys = subItem.IsSys
        //            };

        //            var orders = (subItem?.OrderStr?.Split('-') ?? new[] { subItem.Order + "" }).Select(s => s.ToInt()).Reverse().ToArray();
        //            SetOrder(0, subMenu, orders);



        //            menus.Add(subMenu);

        //            //处理action
        //            if (subItem.ActionEntity != null)
        //            {
        //                if (dic.ContainsKey(subItem.ActionEntity))
        //                {
        //                    var sublist = dic[subItem.ActionEntity];
        //                    if (!sublist.Contains(parentMenu))
        //                        sublist.Add(parentMenu);
        //                    if (!sublist.Contains(subMenu))
        //                        sublist.Add(subMenu);
        //                    dic[subItem.ActionEntity] = sublist;
        //                }
        //                else
        //                {
        //                    dic.Add(subItem.ActionEntity, new List<Menus> { parentMenu, subMenu });
        //                }
        //            }
        //            if (!amenu.Contains(subMenu))
        //                amenu.Add(subMenu);
        //        }
        //        if (pm?.ActionEntity != null)
        //        {
        //            if (!dic.ContainsKey(pm.ActionEntity))
        //            {
        //                dic.Add(pm.ActionEntity, amenu);
        //            }
        //            else
        //            {
        //                dic[pm.ActionEntity] = amenu;
        //            }
        //        }
        //    }
        //    MenuManage.Register(menus, RegisteredBehaviour.Replace);
        //    var actionEntities = ActionManager.GetAllActions(allTypes.ToArray());
        //    foreach (var item in actionEntities)
        //    {
        //        if (!string.IsNullOrWhiteSpace(item.GroupName))
        //        {
        //            var p = Actions.Find(new string[] { "Name", "IsGroup" }, new object[] { item.GroupName, true });
        //            if (p == null)
        //            {
        //                p = new Actions
        //                {
        //                    Name = item.GroupName,
        //                    Description = item.Desc,
        //                    Code = item.Code,
        //                    Remove = false,
        //                    IsSys = item.IsSys,
        //                    IsGroup = true
        //                };
        //                p.Insert();
        //            }
        //            var cur = Actions.Find(new string[] { "Name", "ParentID" }, new object[] { item.Name, p.ID });
        //            if (cur == null)
        //            {
        //                cur = new Actions
        //                {
        //                    Name = item.Name,
        //                    Description = item.Desc,
        //                    Code = item.Code,
        //                    //State = State.Normal,
        //                    Remove = false,
        //                    IsSys = item.IsSys,
        //                    ParentID = p.ID

        //                };
        //                cur.Insert();
        //                //对应菜单
        //                Menus menu = Menus.Find("Name", item.GroupName);
        //                if (menu != null)
        //                {
        //                    var am = MenuActionMap.FindByMenuIDAndActionID(menu.ID, cur.ID);
        //                    if (am == null)
        //                    {
        //                        am = new MenuActionMap();
        //                        am.ActionID = (int)cur.ID;
        //                        am.MenuID = (int)menu.ID;
        //                        am.Insert();
        //                    }
        //                }
        //            }
        //            if (dic.ContainsKey(item))
        //            {
        //                foreach (var m in dic[item])
        //                {
        //                    long id = m.ID;
        //                    if (id == 0)
        //                    {
        //                        var old = Menus.Find("Name", m.Name);
        //                        if (old != null)
        //                            id = old.ID;
        //                    }
        //                    if (id > 0)
        //                    {
        //                        var am = MenuActionMap.FindByMenuIDAndActionID(id, cur.ID);
        //                        if (am == null)
        //                        {
        //                            am = new MenuActionMap();
        //                            am.ActionID = (int)cur.ID;
        //                            am.MenuID = (int)id;
        //                            am.Insert();
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    return Content("菜单更新完成。");
        //}

        ///// <summary>
        ///// 重新设置菜单排序
        ///// </summary>
        ///// <param name="level"></param>
        ///// <param name="mnus"></param>
        ///// <param name="orders"></param>
        //private void SetOrder(int level, Menus mnus, int[] orders)
        //{
        //    var n = orders.Length;

        //    if (n > level)
        //    {
        //        if (mnus != null)
        //        {
        //            mnus.Sort = orders[level];
        //            SetOrder(level + 1, mnus.Parent, orders);
        //        }
        //    }
        //}

        //[ActionDesc("系统设置", "设置系统基本信息", groupName: "系统管理")]
        //[MenuDesc("系统设置", "设置系统基本信息", "系统管理")]
        //public ActionResult Config()
        //{
        //    return View();
        //}
        //[AJAXResult]
        //[HttpPost]
        //[ActionDesc("系统设置", "设置系统基本信息", groupName: "系统管理")]
        //[ValidateInput(false)]
        //[ValidateAntiForgeryToken()]
        //public ActionResult Config(SystemInfo model, SysConfig cofig, string _SMTPPassword)
        //{
        //    if (!_SMTPPassword.IsNullOrWhiteSpace() && _SMTPPassword != "000000")
        //    {
        //        cofig.SMTPPassword = _SMTPPassword;
        //    }
        //    if (!MsgHandler.IsEmail(cofig.MailFrom))
        //    {
        //        Hashtable ht = new Hashtable();
        //        ht.Add("Message", "系统信箱书写不正确，请重新输入！");
        //        ht.Add("Result", false);
        //        return Json(ht);
        //    }
        //    Common.SystemInfo.Name = model.Name;
        //    Common.SystemInfo.SystemConfig = cofig;
        //    Common.SystemInfo.Licensed = model.Licensed;
        //    Common.SystemInfo.Version = model.Version;
        //    Common.SystemInfo.Update();
        //    return Json(new AjaxResult("保存成功"));
        //}

    }
}