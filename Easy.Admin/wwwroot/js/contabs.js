﻿var contabs=function () {
    function f(a) {
        var d = 0;
        return $(a).each(function () {
            d += $(this).outerWidth(!0)
        }), d
    }

    function g(a) {
        var d = f($(a).prevAll()),
            c = f($(a).nextAll()),
            b = f($(".content-tabs").children().not(".J_menuTabs")),
            b = $(".content-tabs").outerWidth(!0) - b,
            e = 0;
        if ($(".page-tabs-content").outerWidth() < b) e = 0;
        else if (c <= b - $(a).outerWidth(!0) - $(a).next().outerWidth(!0)) {
            if (b - $(a).next().outerWidth(!0) > c) for (e = d; e - $(a).outerWidth() > $(".page-tabs-content").outerWidth() - b;) e -= $(a).prev().outerWidth(), a = $(a).prev()
        } else d > b - $(a).outerWidth(!0) - $(a).prev().outerWidth(!0) && (e = d - $(a).prev().outerWidth(!0));
        $(".page-tabs-content").animate({
            marginLeft: 0 - e + "px"
        }, "fast")
    }
    $(".J_menuItem").each(function (a) {
        $(this).attr("data-index") || $(this).attr("data-index", a)
    });
    $(".J_menuItem").on("click", function () {
        var a = $(this).attr("href"),
            d = $(this).data("index"),
            c = $.trim($(this).text()),
            b = !0;
        if (void 0 == a || 0 == $.trim(a).length) return !1;
        if ($(".J_menuTab").each(function () {
            return $(this).data("id") == a ? ($(this).hasClass("active") || ($(this).addClass("active").siblings(".J_menuTab").removeClass("active"), g(this), $(".J_mainContent .J_iframe").each(function () {
                return $(this).data("id") == a ? ($(this).show().siblings(".J_iframe").hide(), !1) : void 0
            })), b = !1, !1) : void 0
        }), b) {
            c = '<a href="javascript:;" class="active J_menuTab" data-id="' + a + '">' + c + ' <i class="fa fa-times-circle"></i></a>';
            $(".J_menuTab").removeClass("active");
            d = '<iframe class="J_iframe" name="iframe' + d + '" width="100%" height="100%" src="' + a + '?v=4.0" frameborder="0" data-id="' + a + '" seamless></iframe>';
            $(".J_mainContent").find("iframe.J_iframe").hide().parents(".J_mainContent").append(d);
            var e = layer.load();
            //$(".J_mainContent iframe:visible").load(function () {
            //    layer.close(e)
            //});
            $(".J_menuTabs .page-tabs-content").append(c);
            g($(".J_menuTab.active"))
        }
        return !1
    });
    $(".J_menuTabs").on("click", ".J_menuTab i", function () {
        var a = $(this).parents(".J_menuTab").data("id"),
            d = $(this).parents(".J_menuTab").width();
        if ($(this).parents(".J_menuTab").hasClass("active")) {
            if ($(this).parents(".J_menuTab").next(".J_menuTab").size()) {
                var c = $(this).parents(".J_menuTab").next(".J_menuTab:eq(0)").data("id");
                $(this).parents(".J_menuTab").next(".J_menuTab:eq(0)").addClass("active");
                $(".J_mainContent .J_iframe").each(function () {
                    return $(this).data("id") == c ? ($(this).show().siblings(".J_iframe").hide(), !1) : void 0
                });
                var b = parseInt($(".page-tabs-content").css("margin-left"));
                0 > b && $(".page-tabs-content").animate({
                    marginLeft: b + d + "px"
                }, "fast");
                $(this).parents(".J_menuTab").remove();
                $(".J_mainContent .J_iframe").each(function () {
                    return $(this).data("id") == a ? ($(this).remove(), !1) : void 0
                })
            }
            $(this).parents(".J_menuTab").prev(".J_menuTab").size() && (c = $(this).parents(".J_menuTab").prev(".J_menuTab:last").data("id"), $(this).parents(".J_menuTab").prev(".J_menuTab:last").addClass("active"), $(".J_mainContent .J_iframe").each(function () {
                return $(this).data("id") == c ? ($(this).show().siblings(".J_iframe").hide(), !1) : void 0
            }), $(this).parents(".J_menuTab").remove(), $(".J_mainContent .J_iframe").each(function () {
                return $(this).data("id") == a ? ($(this).remove(), !1) : void 0
            }))
        } else $(this).parents(".J_menuTab").remove(), $(".J_mainContent .J_iframe").each(function () {
            return $(this).data("id") == a ? ($(this).remove(), !1) : void 0
        }), g($(".J_menuTab.active"));
        return !1
    });
    $(".J_tabCloseOther").on("click", function () {
        $(".page-tabs-content").children("[data-id]").not(":first").not(".active").each(function () {
            $('.J_iframe[data-id="' + $(this).data("id") + '"]').remove();
            $(this).remove()
        });
        $(".page-tabs-content").css("margin-left", "0")
    });
    $(".J_tabShowActive").on("click", function () {
        g($(".J_menuTab.active"))
    });
    $(".J_menuTabs").on("click", ".J_menuTab", function () {
        if (!$(this).hasClass("active")) {
            var a = $(this).data("id");
            $(".J_mainContent .J_iframe").each(function () {
                return $(this).data("id") == a ? ($(this).show().siblings(".J_iframe").hide(), !1) : void 0
            });
            $(this).addClass("active").siblings(".J_menuTab").removeClass("active");
            g(this)
        }
    });
    $(".J_menuTabs").on("dblclick", ".J_menuTab", function () {
        var a = $('.J_iframe[data-id="' + $(this).data("id") + '"]'),
            d = a.attr("src"),
            c = layer.load();
        a.attr("src", d).load(function () {
            layer.close(c)
        })
    });
    $(".J_tabLeft").on("click", function () {
        var a = Math.abs(parseInt($(".page-tabs-content").css("margin-left"))),
            d = f($(".content-tabs").children().not(".J_menuTabs")),
            d = $(".content-tabs").outerWidth(!0) - d,
            c = 0;
        if ($(".page-tabs-content").width() < d) return !1;
        for (var b = $(".J_menuTab:first"), e = 0; e + $(b).outerWidth(!0) <= a;) e += $(b).outerWidth(!0), b = $(b).next();
        if (e = 0, f($(b).prevAll()) > d) {
            for (; e + $(b).outerWidth(!0) < d && 0 < b.length;) e += $(b).outerWidth(!0), b = $(b).prev();
            c = f($(b).prevAll())
        }
        $(".page-tabs-content").animate({
            marginLeft: 0 - c + "px"
        }, "fast")
    });
    $(".J_tabRight").on("click", function () {
        var a = Math.abs(parseInt($(".page-tabs-content").css("margin-left"))),
            d = f($(".content-tabs").children().not(".J_menuTabs")),
            d = $(".content-tabs").outerWidth(!0) - d;
        if ($(".page-tabs-content").width() < d) return !1;
        for (var c = $(".J_menuTab:first"), b = 0; b + $(c).outerWidth(!0) <= a;) b += $(c).outerWidth(!0), c = $(c).next();
        for (b = 0; b + $(c).outerWidth(!0) < d && 0 < c.length;) b += $(c).outerWidth(!0), c = $(c).next();
        a = f($(c).prevAll());
        0 < a && $(".page-tabs-content").animate({
            marginLeft: 0 - a + "px"
        }, "fast")
    });
    $(".J_tabCloseAll").on("click", function () {
        $(".page-tabs-content").children("[data-id]").not(":first").each(function () {
            $('.J_iframe[data-id="' + $(this).data("id") + '"]').remove();
            $(this).remove()
        });
        $(".page-tabs-content").children("[data-id]:first").each(function () {
            $('.J_iframe[data-id="' + $(this).data("id") + '"]').show();
            $(this).addClass("active")
        });
        $(".page-tabs-content").css("margin-left", "0")
    })
}