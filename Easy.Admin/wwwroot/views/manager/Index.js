﻿//渲染菜单

var initMenu = function () {
    $.post("/Manager/GetMenu", null, function (result) {
        if (!result || typeof result !== "object" || result.length < 1) {
            return;
        }
        menuLoadData(result);
        $('#side-menu').metisMenu('dispose');
        //渲染菜单
        $("#side-menu").metisMenu();
        contabs();
    });


    function menuLoadData(treeObj) {
        for (var i = 0; i < treeObj.length; i++) {
            var item = treeObj[i];
            dsei(item);
            if (item.hasChildren) {
                dsei2(item.ChildNodes);
            }
        }
    }
    //一级菜单
    function dsei(data) {
        var ulMenu = $('ul#side-menu');
        var url = data.url ? data.url : "#";
        var arrow = data.hasChildren ? 'class="has-arrow"' : '';
        var zsul = '<li id="' + data.id + '"><a href="' + url + '" ' + arrow+'><i class="fa fa-edit "></i> <span class="nav-label">' + data.name + '</span>' + '</a></li>';
        ulMenu.append(zsul);
    }
    //下级菜单
    function dsei2(data) {
        if (data.length < 1) return;
        var $li = $("li#" + data[0].parentId);//父级li
        var ul = '<ul class="nav nav-second-level"></ul>';//添加子级容器
        $li.append(ul);
        var $ul = $li.find("ul");
        for (var i = 0; i < data.length; i++) {
            var item = data[i];
            var hasChildren = data.hasChildren;
            var url = item.url ? item.url : "#";
            var fallow = hasChildren ? '<span class="fa arrow"></span>' : '';
            var menuTab = hasChildren ? '><i class="fa fa-edit "></i' : 'class="J_menuItem"';
            var zsul = '<li id="' + item.id + '"><a href="' + url + '" ' + menuTab + '>' + fallow + item.name + '</a></li>';
            $ul.append(zsul);
            if (hasChildren) {
                dsei2(item.ChildNodes);
            }
        }
    }
}

$(function () {
    $("#side-menu").metisMenu();
});
$(function () {
    initMenu();
});

//屏幕大于768px时 让左侧菜单显示
var is_mini_navbar = false;
window.onresize = function () {
    if (document.documentElement.clientWidth > 768) {
        if (is_mini_navbar && $("nav[role=navigation]").width() < 220) {
            is_mini_navbar = false;
            $("body").toggleClass("mini-navbar"), SmoothlyMenu();
        }
    } else {
        is_mini_navbar = true;
    }
}

//function isMini() {
//    if (document.documentElement.clientWidth <= 768) {
//        $("body").toggleClass("mini-navbar"), SmoothlyMenu();
//    }
//}

//isMini();

function RegisterMenu() {
    top.layer.msg("正在更新菜单，请稍后！");
    $.get('/Manager/RegisterMenu', {}, function (res) { layer.alert(res) });
}
