﻿using System;
using System.ComponentModel;
using XCode;
using XCode.Configuration;
using XCode.DataAccessLayer;

namespace Gxu.Models
{
    /// <summary>菜单</summary>
    [Serializable]
    [DataObject]
    [Description("菜单")]
    [BindIndex("PK__sys_Menu__3214EC2760A75C0F", true, "ID")]
    [BindTable("Gxu_Menus", Description = "菜单", ConnName = "conn", DbType = DatabaseType.SqlServer)]
    public partial class Menus : IMenus
    {
        #region 属性
        private Int64 _ID;
        /// <summary></summary>
        [DisplayName("ID")]
        [Description("")]
        [DataObjectField(true, true, false, 19)]
        [BindColumn(1, "ID", "", null, "bigint", 19, 0, false)]
        public virtual Int64 ID
        {
            get { return _ID; }
            set { if (OnPropertyChanging(__.ID, value)) { _ID = value; OnPropertyChanged(__.ID); } }
        }

        private String _CreateBy;
        /// <summary></summary>
        [DisplayName("CreateBy")]
        [Description("")]
        [DataObjectField(false, false, false, 20)]
        [BindColumn(2, "CreateBy", "", null, "nvarchar(20)", 0, 0, true)]
        public virtual String CreateBy
        {
            get { return _CreateBy; }
            set { if (OnPropertyChanging(__.CreateBy, value)) { _CreateBy = value; OnPropertyChanged(__.CreateBy); } }
        }

        private DateTime _CreateDate;
        /// <summary></summary>
        [DisplayName("CreateDate")]
        [Description("")]
        [DataObjectField(false, false, false, 3)]
        [BindColumn(3, "CreateDate", "", null, "datetime", 3, 0, false)]
        public virtual DateTime CreateDate
        {
            get { return _CreateDate; }
            set { if (OnPropertyChanging(__.CreateDate, value)) { _CreateDate = value; OnPropertyChanged(__.CreateDate); } }
        }

        private String _ModifyBy;
        /// <summary></summary>
        [DisplayName("ModifyBy")]
        [Description("")]
        [DataObjectField(false, false, true, 20)]
        [BindColumn(4, "ModifyBy", "", null, "nvarchar(20)", 0, 0, true)]
        public virtual String ModifyBy
        {
            get { return _ModifyBy; }
            set { if (OnPropertyChanging(__.ModifyBy, value)) { _ModifyBy = value; OnPropertyChanged(__.ModifyBy); } }
        }

        private DateTime _ModifyDate;
        /// <summary></summary>
        [DisplayName("ModifyDate")]
        [Description("")]
        [DataObjectField(false, false, true, 3)]
        [BindColumn(5, "ModifyDate", "", null, "datetime", 3, 0, false)]
        public virtual DateTime ModifyDate
        {
            get { return _ModifyDate; }
            set { if (OnPropertyChanging(__.ModifyDate, value)) { _ModifyDate = value; OnPropertyChanged(__.ModifyDate); } }
        }

        private Boolean _Remove;
        /// <summary></summary>
        [DisplayName("Remove")]
        [Description("")]
        [DataObjectField(false, false, true, 1)]
        [BindColumn(6, "Remove", "", null, "bit", 0, 0, false)]
        public virtual Boolean Remove
        {
            get { return _Remove; }
            set { if (OnPropertyChanging(__.Remove, value)) { _Remove = value; OnPropertyChanged(__.Remove); } }
        }

        private String _RemoveBy;
        /// <summary></summary>
        [DisplayName("RemoveBy")]
        [Description("")]
        [DataObjectField(false, false, true, 20)]
        [BindColumn(7, "RemoveBy", "", null, "nvarchar(20)", 0, 0, true)]
        public virtual String RemoveBy
        {
            get { return _RemoveBy; }
            set { if (OnPropertyChanging(__.RemoveBy, value)) { _RemoveBy = value; OnPropertyChanged(__.RemoveBy); } }
        }

        private DateTime _RemoveDate;
        /// <summary></summary>
        [DisplayName("RemoveDate")]
        [Description("")]
        [DataObjectField(false, false, true, 3)]
        [BindColumn(8, "RemoveDate", "", null, "datetime", 3, 0, false)]
        public virtual DateTime RemoveDate
        {
            get { return _RemoveDate; }
            set { if (OnPropertyChanging(__.RemoveDate, value)) { _RemoveDate = value; OnPropertyChanged(__.RemoveDate); } }
        }

        private String _Name;
        /// <summary>菜单名称</summary>
        [DisplayName("菜单名称")]
        [Description("菜单名称")]
        [DataObjectField(false, false, true, 50)]
        [BindColumn(9, "Name", "菜单名称", null, "nvarchar(50)", 0, 0, true)]
        public virtual String Name
        {
            get { return _Name; }
            set { if (OnPropertyChanging(__.Name, value)) { _Name = value; OnPropertyChanged(__.Name); } }
        }

        private String _ParentIds;
        /// <summary>所有父级菜单id</summary>
        [DisplayName("所有父级菜单id")]
        [Description("所有父级菜单id")]
        [DataObjectField(false, false, true, 255)]
        [BindColumn(10, "ParentIds", "所有父级菜单id", null, "nvarchar(255)", 0, 0, true)]
        public virtual String ParentIds
        {
            get { return _ParentIds; }
            set { if (OnPropertyChanging(__.ParentIds, value)) { _ParentIds = value; OnPropertyChanged(__.ParentIds); } }
        }

        private String _Description;
        /// <summary>描述</summary>
        [DisplayName("描述")]
        [Description("描述")]
        [DataObjectField(false, false, true, 255)]
        [BindColumn(11, "Description", "描述", null, "nvarchar(255)", 0, 0, true)]
        public virtual String Description
        {
            get { return _Description; }
            set { if (OnPropertyChanging(__.Description, value)) { _Description = value; OnPropertyChanged(__.Description); } }
        }

        private String _Code;
        /// <summary></summary>
        [DisplayName("Code")]
        [Description("")]
        [DataObjectField(false, false, true, 50)]
        [BindColumn(12, "Code", "", null, "nvarchar(50)", 0, 0, true)]
        public virtual String Code
        {
            get { return _Code; }
            set { if (OnPropertyChanging(__.Code, value)) { _Code = value; OnPropertyChanged(__.Code); } }
        }

        private String _Controller;
        /// <summary></summary>
        [DisplayName("Controller")]
        [Description("")]
        [DataObjectField(false, false, true, 50)]
        [BindColumn(13, "Controller", "", null, "nvarchar(50)", 0, 0, true)]
        public virtual String Controller
        {
            get { return _Controller; }
            set { if (OnPropertyChanging(__.Controller, value)) { _Controller = value; OnPropertyChanged(__.Controller); } }
        }

        private String _Action;
        /// <summary></summary>
        [DisplayName("Action")]
        [Description("")]
        [DataObjectField(false, false, true, 50)]
        [BindColumn(14, "Action", "", null, "nvarchar(50)", 0, 0, true)]
        public virtual String Action
        {
            get { return _Action; }
            set { if (OnPropertyChanging(__.Action, value)) { _Action = value; OnPropertyChanged(__.Action); } }
        }

        private String _Parameters;
        /// <summary></summary>
        [DisplayName("Parameters")]
        [Description("")]
        [DataObjectField(false, false, true, 255)]
        [BindColumn(15, "Parameters", "", null, "nvarchar(255)", 0, 0, true)]
        public virtual String Parameters
        {
            get { return _Parameters; }
            set { if (OnPropertyChanging(__.Parameters, value)) { _Parameters = value; OnPropertyChanged(__.Parameters); } }
        }

        private Int32 _Sort;
        /// <summary></summary>
        [DisplayName("Sort")]
        [Description("")]
        [DataObjectField(false, false, true, 10)]
        [BindColumn(16, "Sort", "", null, "int", 10, 0, false)]
        public virtual Int32 Sort
        {
            get { return _Sort; }
            set { if (OnPropertyChanging(__.Sort, value)) { _Sort = value; OnPropertyChanged(__.Sort); } }
        }

        private Int32 _Render;
        /// <summary></summary>
        [DisplayName("Render")]
        [Description("")]
        [DataObjectField(false, false, true, 10)]
        [BindColumn(17, "Render", "", null, "int", 10, 0, false)]
        public virtual Int32 Render
        {
            get { return _Render; }
            set { if (OnPropertyChanging(__.Render, value)) { _Render = value; OnPropertyChanged(__.Render); } }
        }

        private Int32 _Depth;
        /// <summary></summary>
        [DisplayName("Depth")]
        [Description("")]
        [DataObjectField(false, false, true, 10)]
        [BindColumn(18, "Depth", "", null, "int", 10, 0, false)]
        public virtual Int32 Depth
        {
            get { return _Depth; }
            set { if (OnPropertyChanging(__.Depth, value)) { _Depth = value; OnPropertyChanged(__.Depth); } }
        }

        private Int32 _ChildNum;
        /// <summary></summary>
        [DisplayName("ChildNum")]
        [Description("")]
        [DataObjectField(false, false, true, 10)]
        [BindColumn(19, "ChildNum", "", null, "int", 10, 0, false)]
        public virtual Int32 ChildNum
        {
            get { return _ChildNum; }
            set { if (OnPropertyChanging(__.ChildNum, value)) { _ChildNum = value; OnPropertyChanged(__.ChildNum); } }
        }

        private String _URL;
        /// <summary></summary>
        [DisplayName("Url")]
        [Description("")]
        [DataObjectField(false, false, true, 255)]
        [BindColumn(20, "URL", "", null, "nvarchar(255)", 0, 0, true)]
        public virtual String URL
        {
            get { return _URL; }
            set { if (OnPropertyChanging(__.URL, value)) { _URL = value; OnPropertyChanged(__.URL); } }
        }

        private String _ICON;
        /// <summary></summary>
        [DisplayName("Icon")]
        [Description("")]
        [DataObjectField(false, false, true, 255)]
        [BindColumn(21, "ICON", "", null, "nvarchar(255)", 0, 0, true)]
        public virtual String ICON
        {
            get { return _ICON; }
            set { if (OnPropertyChanging(__.ICON, value)) { _ICON = value; OnPropertyChanged(__.ICON); } }
        }

        private Int64 _ParentID;
        /// <summary></summary>
        [DisplayName("ParentID")]
        [Description("")]
        [DataObjectField(false, false, true, 19)]
        [BindColumn(22, "ParentID", "", null, "bigint", 19, 0, false)]
        public virtual Int64 ParentID
        {
            get { return _ParentID; }
            set { if (OnPropertyChanging(__.ParentID, value)) { _ParentID = value; OnPropertyChanged(__.ParentID); } }
        }

        private String _State;
        /// <summary></summary>
        [DisplayName("State")]
        [Description("")]
        [DataObjectField(false, false, true, 50)]
        [BindColumn(23, "State", "", null, "nvarchar(50)", 0, 0, true)]
        public virtual String State
        {
            get { return _State; }
            set { if (OnPropertyChanging(__.State, value)) { _State = value; OnPropertyChanged(__.State); } }
        }

        private Boolean _IsChild;
        /// <summary></summary>
        [DisplayName("IsChild")]
        [Description("")]
        [DataObjectField(false, false, true, 1)]
        [BindColumn(24, "IsChild", "", null, "bit", 0, 0, false)]
        public virtual Boolean IsChild
        {
            get { return _IsChild; }
            set { if (OnPropertyChanging(__.IsChild, value)) { _IsChild = value; OnPropertyChanged(__.IsChild); } }
        }

        private Boolean _IsSys;
        /// <summary>是否系统菜单</summary>
        [DisplayName("是否系统菜单")]
        [Description("是否系统菜单")]
        [DataObjectField(false, false, true, 1)]
        [BindColumn(25, "IsSys", "是否系统菜单", null, "bit", 0, 0, false)]
        public virtual Boolean IsSys
        {
            get { return _IsSys; }
            set { if (OnPropertyChanging(__.IsSys, value)) { _IsSys = value; OnPropertyChanged(__.IsSys); } }
        }
        #endregion

        #region 获取/设置 字段值
        /// <summary>
        /// 获取/设置 字段值。
        /// 一个索引，基类使用反射实现。
        /// 派生实体类可重写该索引，以避免反射带来的性能损耗
        /// </summary>
        /// <param name="name">字段名</param>
        /// <returns></returns>
        public override Object this[String name]
        {
            get
            {
                switch (name)
                {
                    case __.ID : return _ID;
                    case __.CreateBy : return _CreateBy;
                    case __.CreateDate : return _CreateDate;
                    case __.ModifyBy : return _ModifyBy;
                    case __.ModifyDate : return _ModifyDate;
                    case __.Remove : return _Remove;
                    case __.RemoveBy : return _RemoveBy;
                    case __.RemoveDate : return _RemoveDate;
                    case __.Name : return _Name;
                    case __.ParentIds : return _ParentIds;
                    case __.Description : return _Description;
                    case __.Code : return _Code;
                    case __.Controller : return _Controller;
                    case __.Action : return _Action;
                    case __.Parameters : return _Parameters;
                    case __.Sort : return _Sort;
                    case __.Render : return _Render;
                    case __.Depth : return _Depth;
                    case __.ChildNum : return _ChildNum;
                    case __.URL : return _URL;
                    case __.ICON : return _ICON;
                    case __.ParentID : return _ParentID;
                    case __.State : return _State;
                    case __.IsChild : return _IsChild;
                    case __.IsSys : return _IsSys;
                    default: return base[name];
                }
            }
            set
            {
                switch (name)
                {
                    case __.ID : _ID = Convert.ToInt64(value); break;
                    case __.CreateBy : _CreateBy = Convert.ToString(value); break;
                    case __.CreateDate : _CreateDate = Convert.ToDateTime(value); break;
                    case __.ModifyBy : _ModifyBy = Convert.ToString(value); break;
                    case __.ModifyDate : _ModifyDate = Convert.ToDateTime(value); break;
                    case __.Remove : _Remove = Convert.ToBoolean(value); break;
                    case __.RemoveBy : _RemoveBy = Convert.ToString(value); break;
                    case __.RemoveDate : _RemoveDate = Convert.ToDateTime(value); break;
                    case __.Name : _Name = Convert.ToString(value); break;
                    case __.ParentIds : _ParentIds = Convert.ToString(value); break;
                    case __.Description : _Description = Convert.ToString(value); break;
                    case __.Code : _Code = Convert.ToString(value); break;
                    case __.Controller : _Controller = Convert.ToString(value); break;
                    case __.Action : _Action = Convert.ToString(value); break;
                    case __.Parameters : _Parameters = Convert.ToString(value); break;
                    case __.Sort : _Sort = Convert.ToInt32(value); break;
                    case __.Render : _Render = Convert.ToInt32(value); break;
                    case __.Depth : _Depth = Convert.ToInt32(value); break;
                    case __.ChildNum : _ChildNum = Convert.ToInt32(value); break;
                    case __.URL : _URL = Convert.ToString(value); break;
                    case __.ICON : _ICON = Convert.ToString(value); break;
                    case __.ParentID : _ParentID = Convert.ToInt64(value); break;
                    case __.State : _State = Convert.ToString(value); break;
                    case __.IsChild : _IsChild = Convert.ToBoolean(value); break;
                    case __.IsSys : _IsSys = Convert.ToBoolean(value); break;
                    default: base[name] = value; break;
                }
            }
        }
        #endregion

        #region 字段名
        /// <summary>取得菜单字段信息的快捷方式</summary>
        public partial class _
        {
            ///<summary></summary>
            public static readonly Field ID = FindByName(__.ID);

            ///<summary></summary>
            public static readonly Field CreateBy = FindByName(__.CreateBy);

            ///<summary></summary>
            public static readonly Field CreateDate = FindByName(__.CreateDate);

            ///<summary></summary>
            public static readonly Field ModifyBy = FindByName(__.ModifyBy);

            ///<summary></summary>
            public static readonly Field ModifyDate = FindByName(__.ModifyDate);

            ///<summary></summary>
            public static readonly Field Remove = FindByName(__.Remove);

            ///<summary></summary>
            public static readonly Field RemoveBy = FindByName(__.RemoveBy);

            ///<summary></summary>
            public static readonly Field RemoveDate = FindByName(__.RemoveDate);

            ///<summary>菜单名称</summary>
            public static readonly Field Name = FindByName(__.Name);

            ///<summary>所有父级菜单id</summary>
            public static readonly Field ParentIds = FindByName(__.ParentIds);

            ///<summary>描述</summary>
            public static readonly Field Description = FindByName(__.Description);

            ///<summary></summary>
            public static readonly Field Code = FindByName(__.Code);

            ///<summary></summary>
            public static readonly Field Controller = FindByName(__.Controller);

            ///<summary></summary>
            public static readonly Field Action = FindByName(__.Action);

            ///<summary></summary>
            public static readonly Field Parameters = FindByName(__.Parameters);

            ///<summary></summary>
            public static readonly Field Sort = FindByName(__.Sort);

            ///<summary></summary>
            public static readonly Field Render = FindByName(__.Render);

            ///<summary></summary>
            public static readonly Field Depth = FindByName(__.Depth);

            ///<summary></summary>
            public static readonly Field ChildNum = FindByName(__.ChildNum);

            ///<summary></summary>
            public static readonly Field URL = FindByName(__.URL);

            ///<summary></summary>
            public static readonly Field ICON = FindByName(__.ICON);

            ///<summary></summary>
            public static readonly Field ParentID = FindByName(__.ParentID);

            ///<summary></summary>
            public static readonly Field State = FindByName(__.State);

            ///<summary></summary>
            public static readonly Field IsChild = FindByName(__.IsChild);

            ///<summary>是否系统菜单</summary>
            public static readonly Field IsSys = FindByName(__.IsSys);

            static Field FindByName(String name) { return Entity<Menus>.Meta.Table.FindByName(name); }
        }

        /// <summary>取得菜单字段名称的快捷方式</summary>
        partial class __
        {
            ///<summary></summary>
            public const String ID = "ID";

            ///<summary></summary>
            public const String CreateBy = "CreateBy";

            ///<summary></summary>
            public const String CreateDate = "CreateDate";

            ///<summary></summary>
            public const String ModifyBy = "ModifyBy";

            ///<summary></summary>
            public const String ModifyDate = "ModifyDate";

            ///<summary></summary>
            public const String Remove = "Remove";

            ///<summary></summary>
            public const String RemoveBy = "RemoveBy";

            ///<summary></summary>
            public const String RemoveDate = "RemoveDate";

            ///<summary>菜单名称</summary>
            public const String Name = "Name";

            ///<summary>所有父级菜单id</summary>
            public const String ParentIds = "ParentIds";

            ///<summary>描述</summary>
            public const String Description = "Description";

            ///<summary></summary>
            public const String Code = "Code";

            ///<summary></summary>
            public const String Controller = "Controller";

            ///<summary></summary>
            public const String Action = "Action";

            ///<summary></summary>
            public const String Parameters = "Parameters";

            ///<summary></summary>
            public const String Sort = "Sort";

            ///<summary></summary>
            public const String Render = "Render";

            ///<summary></summary>
            public const String Depth = "Depth";

            ///<summary></summary>
            public const String ChildNum = "ChildNum";

            ///<summary></summary>
            public const String URL = "URL";

            ///<summary></summary>
            public const String ICON = "ICON";

            ///<summary></summary>
            public const String ParentID = "ParentID";

            ///<summary></summary>
            public const String State = "State";

            ///<summary></summary>
            public const String IsChild = "IsChild";

            ///<summary>是否系统菜单</summary>
            public const String IsSys = "IsSys";

        }
        #endregion
    }

    /// <summary>菜单接口</summary>
    public partial interface IMenus
    {
        #region 属性
        /// <summary></summary>
        Int64 ID { get; set; }

        /// <summary></summary>
        String CreateBy { get; set; }

        /// <summary></summary>
        DateTime CreateDate { get; set; }

        /// <summary></summary>
        String ModifyBy { get; set; }

        /// <summary></summary>
        DateTime ModifyDate { get; set; }

        /// <summary></summary>
        Boolean Remove { get; set; }

        /// <summary></summary>
        String RemoveBy { get; set; }

        /// <summary></summary>
        DateTime RemoveDate { get; set; }

        /// <summary>菜单名称</summary>
        String Name { get; set; }

        /// <summary>所有父级菜单id</summary>
        String ParentIds { get; set; }

        /// <summary>描述</summary>
        String Description { get; set; }

        /// <summary></summary>
        String Code { get; set; }

        /// <summary></summary>
        String Controller { get; set; }

        /// <summary></summary>
        String Action { get; set; }

        /// <summary></summary>
        String Parameters { get; set; }

        /// <summary></summary>
        Int32 Sort { get; set; }

        /// <summary></summary>
        Int32 Render { get; set; }

        /// <summary></summary>
        Int32 Depth { get; set; }

        /// <summary></summary>
        Int32 ChildNum { get; set; }

        /// <summary></summary>
        String URL { get; set; }

        /// <summary></summary>
        String ICON { get; set; }

        /// <summary></summary>
        Int64 ParentID { get; set; }

        /// <summary></summary>
        String State { get; set; }

        /// <summary></summary>
        Boolean IsChild { get; set; }

        /// <summary>是否系统菜单</summary>
        Boolean IsSys { get; set; }
        #endregion

        #region 获取/设置 字段值
        /// <summary>获取/设置 字段值。</summary>
        /// <param name="name">字段名</param>
        /// <returns></returns>
        Object this[String name] { get; set; }
        #endregion
    }
}